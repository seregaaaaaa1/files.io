﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace Files
{
    class Program
    {
        static void Main(string[] args)
        {
            //проверить если директории не существует, то создать её и создать в директории текстовый документ
            const string pathDirectory = @"p:\logs\";
            const int fileSizeLimit = 1000000;
            var amountOfFiles = 0;
            var directory = new DirectoryInfo(pathDirectory);
            if (!directory.Exists)
            {
                directory.Create();
            }

            // var stream = new FileStream(pathFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            //в директории найти последний изменённый файл
            //для 5ти итераций:
            //открыть последний изменённый файл для записи
            //записать в открытый файл n байт
            Random random = new Random();

            while (amountOfFiles <= 5)
            {
                using (FileStream fs = File.Create(GenerateFileName(pathDirectory)))
                {
                    while (fs.Length < fileSizeLimit)
                    {
                        var value = random.Next(100).ToString();

                        AddText(fs, value);
                       
                    }
                    //Thread.Sleep(1500);
                }
                amountOfFiles++;
            }
            //проверить если предыдущий файл заполнен на 100 байтов, то закрыть поток и создать новый текстовый документ, открыть его для чтения
            //записать в открытый файл 100 байт


            // Delete the file if it exists.
            //if (File.Exists(path))
            //{
            //    File.Delete(path);
            //}
            
        }

        // byte[] result = uniencoding.GetBytes(UserInput.Text);

        //using (FileStream SourceStream = File.Open(filename, FileMode.OpenOrCreate))
        //{
        //    SourceStream.Seek(0, SeekOrigin.End);
        //    await SourceStream.WriteAsync(result, 0, result.Length);
        //}
        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }

        private static string GenerateFileName(string path) => path + DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss-fff tt") + ".txt";
    }
}

